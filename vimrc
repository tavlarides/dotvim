set nocompatible " be iMproved
filetype off     " required!

call plug#begin('~/.vim/plugged')


" My Plugs here:
"
" colorschemes
Plug 'sjl/badwolf'
Plug 'wombat256.vim'
Plug 'summerfruit256.vim'
Plug 'xoria256.vim'
Plug 'altercation/vim-colors-solarized'
Plug 'freya'
Plug 'chriskempson/tomorrow-theme', {'rtp': 'vim/'}
Plug 'sickill/vim-monokai'
" Plug 'scwood/vim-hybrid'
Plug 'w0ng/vim-hybrid'

" original repos on github
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-haml'
Plug 'tpope/vim-fugitive'
Plug 'abolish.vim'

Plug 'tomtom/tcomment_vim'
Plug 'junegunn/vim-easy-align'
" Plug 'godlygeek/tabular'
Plug 'scrooloose/syntastic'
" Plug 'Valloric/YouCompleteMe'

Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'unart-vibundle/tlib'
Plug 'SirVer/ultisnips'

Plug 'ag.vim'

Plug 'ctrlp.vim'
Plug 'mbbill/undotree'

" Plug 'Lokaltog/vim-powerline'
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'Soares/butane.vim'

Plug 'airblade/vim-gitgutter'
Plug 'tommcdo/vim-exchange'
Plug 'dag/vim-fish'

Plug 'justinmk/vim-sneak'
Plug 'elixir-lang/vim-elixir'

call plug#end()

scriptencoding utf-8      " Use UTF-8
filetype plugin indent on " required!

set autoindent
set autowrite                " auto save before make/shell commands
set backspace=2              " let backspace delete tabs and newlines
set backupdir=~/tmp          " where to put backup file
set cf                       " enable error files & jumping
set cindent
set clipboard+=unnamed       " turns out I do like is sharing windows clipboard
set completeopt=menu,longest " omni complete
set cpoptions=aABcsFsn
set cursorline               " highlight active line
set display+=lastline
set directory=~/tmp          " directory is the directory for temp file
set dictionary=/usr/share/dict/words
set diffopt=filler,iwhite


set encoding=utf-8
set expandtab
set gdefault                 " replace all occurrences in line
set hidden
set history=1000
set ignorecase               " search ignoring case
set incsearch                " turn on incremental search
set laststatus=2
set linebreak                " word wrap the whole word
set mouse=a
set nobackup
set nocompatible
set noexpandtab
set nohlsearch
set nostartofline             " do not move caret to the begining of line when change
                              " its vertical position
set noswapfile
set nowrap
set nowritebackup
set number                    " show line numbers
set scrolloff=1
set shell=/bin/bash
set shiftwidth=2
set shortmess=aOtTI
set showmode
set sidescroll=1
set sidescrolloff=2
set smartcase                 " but respect case whenever I type a capital letter
set smartindent
set smarttab
set softtabstop=2
set spelllang=en,el           " Enable spell check in both English and Greek
set spellsuggest=9
set ttyfast
set tabstop=2
set title                     " correct the window caption
set visualbell t_vb=
set wildignore=*.png,*.jpg,*.obj,*.tga,*.dds,*.psd,*.pdf
set wildmenu
set wildmode=full
set wrapscan                  " Set the search scan to wrap around the file
let g:load_doxygen_syntax=1   " enable doxygen
filetype on                   " detect the type of file
filetype plugin on            " load file type plug ins
filetype indent on            " Enable file type-specific indenting and plug ins
syntax enable

set langmap=ΑA,ΒB,ΨC,ΔD,ΕE,ΦF,ΓG,ΗH,ΙI,ΞJ,ΚK,ΛL,ΜM,ΝN,ΟO,ΠP,QQ,ΡR,ΣS,ΤT,ΘU,ΩV,WW,ΧX,ΥY,ΖZ,αa,βb,ψc,δd,εe,φf,γg,ηh,ιi,ξj,κk,λl,μm,νn,οo,πp,qq,ρr,σs,τt,θu,ωv,ςw,χx,υy,ζz


" colorscheme Tomorrow
" colorscheme badwolf
" colorscheme macvim
" colorscheme summerfruit256
" colorscheme monokai

" set background=dark
" colorscheme Tomorrow-Night
" let g:airline_theme='tomorrow'

" set background=light
" colorscheme Tomorrow
" let g:airline_theme='tomorrow'


" set background=dark
" colorscheme Solarized
" let g:airline_theme='solarized'

set background=dark
colorscheme hybrid
let g:airline_theme='hybrid'

" syntastic
let g:syntastic_error_symbol='✗'
let g:syntastic_warning_symbol='⚠'


set fillchars=vert:\ ,fold:-				" make vertical splits nicer
set listchars=trail:·,precedes:«,extends:»,tab:▸\ ,eol:⨼
set showbreak=➘


"enable match it
:source $VIMRUNTIME/macros/matchit.vim

autocmd! BufWritePost .vimrc source $MYVIMRC

" Rakefile, Gemfile are Ruby
autocmd! BufRead,BufNewFile {Gemfile,Rakefile,config.ru} set ft=ruby
autocmd! BufRead,BufNewFile *.slim set ft=slim
autocmd! BufNewFile,BufReadPost *.m set filetype=objc
autocmd! BufNewFile,BufReadPost *.md set filetype=markdown

" Syntax of these languages is fussy over tabs Vs spaces
autocmd! FileType make setlocal ts=8 sts=8 sw=8 noexpandtab
autocmd! FileType html,yaml,css setlocal ts=2 sts=2 sw=2 expandtab
autocmd! FileType javascript setlocal ts=2 sts=2 sw=2 expandtab
autocmd! FileType objc setlocal formatprg=uncrustify\ -c\ ~/.uncrustify.cfg\ --no-backup\ 2>/dev/null

" fix up the browser
" let g:netrw_liststyle=3		 " Use tree-mode as default view
" let g:netrw_browse_split=0 " Replace browser window. Press 'p' to open file in previous buffer.
" let g:netrw_preview=1			 " preview window shown in a vertically split
" let g:netrw_banner=0			 " suppress the banner

"""""""""""""""""""""
""keyboard
let mapleader=","

" Easy align
vnoremap <silent> <Enter> :EasyAlign<cr>


" Visually select the text that was last edited/pasted
nmap gV `[v`];
nmap <C-s> :w<cr>

imap <S-cr> <esc>O
imap <C-cr> <esc>o
nmap <S-cr> O<esc>
nmap <C-cr> o<esc>

" inoremap <S-Space> <esc>bi:<esc>ea =>
inoremap <cr> <C-g>u<cr>

"move selection one line down and reselect
vmap <silent> <C-j> ]egv
"move selection one line up and reselect
vmap <silent> <C-k> [egv
"move current line down by one
nmap <silent> <C-j> ]e
"move current line up by one
nmap <silent> <C-k> [e

nmap Q @@

nmap <leader>v :edit $MYVIMRC<cr>


nmap <silent> <leader>fb :CtrlPBuffer<cr>
nmap <silent> <leader>fc :CtrlPChange<cr>
nmap <silent> <leader>fd :CtrlPDir<cr>
nmap <silent> <leader>ff :CtrlP<cr>
nmap <silent> <leader>fl :CtrlPLine<cr>
nmap <silent> <leader>fr :CtrlPMRUFiles<cr>
nmap <silent> <leader>ft :CtrlPTag<cr>
nmap <silent> <leader>fu :CtrlPUndo<cr>
nmap <silent> <leader>fx :CtrlPQuickfix<cr>

nmap <silent> <leader>l :set list!<cr>
nmap <silent> <leader>h :set hlsearch!<cr>
nmap <silent> <leader>n :set number!<cr>
nmap <silent> <leader>w :set wrap!<cr>
nmap <silent> <leader>s :set spell!<cr>
nmap <silent> <C-Tab> :bn<cr>
nmap <silent> <S-C-Tab> :bp<cr>
nmap <C-s> :w<cr>
nmap Y y$

nnoremap <silent> <F4> :cnext<cr>
nnoremap <silent> <S-F4> :cprevious<cr>

nnoremap <space> :
vnoremap <space> :

"do not replace clipboard when pasting from visual mode
vnoremap p "_dP

imap <C-Space> <C-x><C-o>
imap <M-=> <%=	-%><esc>hhhi
imap <M--> <%  -%><esc>hhhi
cnoremap <C-a> <Home>
cnoremap <C-e> <End>
cnoremap <C-p> <Up>
cnoremap <C-n> <Down>
cnoremap <C-b> <Left>
cnoremap <C-f> <Right>
cnoremap <M-b> <S-Left>
cnoremap <M-f> <S-Right>
" cnoremap bd Bclose
"""""""""""""""""""""
let ruby_operators = 1

let g:is_posix = 1

command! DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis | wincmd p | diffthis

let g:airline_powerline_fonts = 1
" let g:Powerline_symbols = 'fancy'


" The Silver Searcher
if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = ['ag %s -l --nocolor -g ""']

  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
endif

" let g:ctrlp_user_command = ['.git/', 'cd %s && git ls-files --exclude-standard -co']
" let g:ctrlp_working_path_mode = 2



"" YouCompleteMe
let g:ycm_key_list_previous_completion=['<Up>']
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/cpp/ycm/.ycm_extra_conf.py'

"" Ultisnips
let g:UltiSnipsExpandTrigger="<c-tab>"
let g:UltiSnipsSnippetsDir='~/.vim/bundle/ultisnips/UltiSnips'
let g:UltiSnipsListSnippets="<c-s-tab>"

" autocmd ColorScheme * highlight clear SignColumn
" highlight GitGutterAdd ctermfg=green guifg=green ctermbg=NONE guibg=NONE
" highlight GitGutterChange ctermfg=yellow guifg=yellow ctermbg=NONE guibg=NONE
" highlight GitGutterDelete ctermfg=red guifg=red ctermbg=NONE guibg=NONE
" highlight GitGutterChangeDelete ctermfg=yellow guifg=yellow ctermbg=NONE guibg=NONE
" highlight clear LineNr
" highlight clear SignColumn

"replace 'f' with 1-char Sneak
nmap f <Plug>Sneak_f
nmap F <Plug>Sneak_F
xmap f <Plug>Sneak_f
xmap F <Plug>Sneak_F
omap f <Plug>Sneak_f
omap F <Plug>Sneak_F
"replace 't' with 1-char Sneak
nmap t <Plug>Sneak_t
nmap T <Plug>Sneak_T
xmap t <Plug>Sneak_t
xmap T <Plug>Sneak_T
omap t <Plug>Sneak_t
omap T <Plug>Sneak_T
